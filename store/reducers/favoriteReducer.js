const initialState = { favoritesFilm: [] }

function toggleFavorite(state = initialState, action) {
    let nextState
    switch (action.type) {
        case 'TOGGLE_FAVORITE':
            const favoriteFilmIndex = state.favoritesFilm.findIndex(item => item.id === action.value.id)

            if (favoriteFilmIndex !== -1) {
                // Le film est déjà dan sles favoris, on el esupprime de la liste
                nextState = {
                    ...state,
                    favoritesFilm: state.favoritesFilm.filter( (item, index) => index !== favoriteFilmIndex )
                }
            }
            else {
                // Le fil n'est pas dans les films favorite, on l'ajoute à la liste
                nextState = {
                    ...state,
                    favoritesFilm: [...state.favoritesFilm, action.value]
                }
            }
            return nextState || state
    default:
        return state
    }
}

export default toggleFavorite 