import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

import { getImageFromApi } from '../API/TMBDApi'
import FadeIn from '../animations/FadeIn'

class FilmItem extends React.Component {

    _displayFavoriteImage() {
        if (this.props.isFilmFavorite) {
            return (
                <Image style={styles.favorite_image} source={require('../images/ic_favorite.png')}/>
            )
        }
    }

    render() {
        const { film, displayDetailForFilm } = this.props
        return(
            <FadeIn>
                <TouchableOpacity 
                    style={styles.main_container}
                    onPress={() => displayDetailForFilm(film.id)}
                >
                    <Image 
                        style={styles.film_image}
                        source={{ uri: getImageFromApi(film.poster_path) }}
                    />
                    <View style={styles.content}>
                        <View style={styles.header}>
                            {this._displayFavoriteImage()}
                            <Text style={styles.title_text}>{film.title}</Text>
                            <Text style={styles.vote_text}>{film.vote_average}</Text>
                        </View>
                        <View style={styles.description}>
                            <Text style={styles.description_text} numberOfLines={6}>{film.overview}</Text>
                        </View>
                        <View style={styles.date}>
                            <Text style={styles.date_text}>Sorti le {film.release_date}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </FadeIn>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        height: 190,
        flexDirection: 'row',
    },
    film_image: {
        width: 120,
        height: 180,
        margin: 5,
    },
    content: {
        flex: 1,
        margin: 5
    },
    header: {
        flexDirection: 'row',
        flex: 3
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 20,
        flex: 1,
        flexWrap: 'wrap',
        paddingRight: 5
    },
    vote_text: {
        fontWeight: 'bold',
        fontSize: 26,
        color: '#666666'
    },
    description: {
        flex: 7
    },
    description_text: {
        fontStyle: 'italic',
        color: '#4A4A4A'
    },
    date: {
        flex: 1
    },
    date_text: {
        textAlign: 'right',
        fontSize: 14
    },
    favorite_image: {
        width: 25,
        height: 25,
        marginRight: 5
    }

})

export default FilmItem;